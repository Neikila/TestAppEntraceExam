package com.example.neikila.mapviewerwithcache.myCustomMapView.asyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.neikila.mapviewerwithcache.myCustomMapView.DefaultTileManager;
import com.example.neikila.mapviewerwithcache.myCustomMapView.ReusableBitmapFactory;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import utils.Web;
import utils.messages.ErrorMessage;

/**
 * Created by neikila on 21.09.16.
 */
public class GetBitmapAsyncTask extends AsyncTask<Void, GetBitmapAsyncTask.Result, Void> {
    private WeakReference<DefaultTileManager> reference;
    private DefaultTileManager.UrlProvider urlProvider;
    private BitmapFactory.Options options;
    private WeakReference<ReusableBitmapFactory> factoryRef;
    private int maxTries = 3;

    private Point point;
    private List<Point> points;

    public GetBitmapAsyncTask(WeakReference<DefaultTileManager> reference,
                              DefaultTileManager.UrlProvider urlProvider, Point point) {
        this.urlProvider = urlProvider;
        this.reference = reference;
        this.point = point;
    }

    public GetBitmapAsyncTask(WeakReference<DefaultTileManager> reference,
                              DefaultTileManager.UrlProvider urlProvider, List<Point> points) {
        this.urlProvider = urlProvider;
        this.reference = reference;
        this.points = points;
    }

    public GetBitmapAsyncTask setBitmapOptions(BitmapFactory.Options options) {
        this.options = options;
        return this;
    }

    public GetBitmapAsyncTask setMaxTries(int maxTries) {
        this.maxTries = maxTries;
        return this;
    }

    public GetBitmapAsyncTask setReusableBitmapFactory(WeakReference<ReusableBitmapFactory> factoryRef) {
        this.factoryRef = factoryRef;
        return this;
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (factoryRef == null && options == null) {
            options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inMutable = true;
            options.inSampleSize = 1;
        }

        if (point != null)
            makeRequest(point);

        if (points != null)
            for(Point p: points)
                makeRequest(p);

        return null;
    }

    private void makeRequest(Point point) {
        Result result = new Result(point);
        String url = urlProvider.getUrl(point);

        OkHttpClient client = Web.getClient();

        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().noCache().build())
                .url(url)
                .build();

        Response response;
        int counter = 0;
        try {
            while (result.bitmap == null && counter < maxTries) {
                response = client.newCall(request).execute();

                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
                result.bytes = response.body().bytes();
                response.close();

                ReusableBitmapFactory factory;
                if (factoryRef != null && (factory = factoryRef.get()) != null) {
                    result.bitmap = factory.createBitmapFor(result.bytes);
                } else {
                    if (options != null)
                        result.bitmap = BitmapFactory.decodeByteArray(result.bytes, 0, result.bytes.length, options);
                }
            }
            if (counter == maxTries)
                result.errorType = ErrorMessage.ErrorType.BadData;
        } catch (SocketTimeoutException ex) {
            ex.printStackTrace();
            result.errorType = ErrorMessage.ErrorType.TimeoutException;
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
            result.errorType = ErrorMessage.ErrorType.UnknownHost;
        } catch (ConnectException ex) {
            ex.printStackTrace();
            result.errorType = ErrorMessage.ErrorType.NoConnection;
        } catch (IOException ex) {
            ex.printStackTrace();
            result.errorType = ErrorMessage.ErrorType.InternetError;
        }
        publishProgress(result);
    }


    @Override
    protected final void onProgressUpdate(Result... values) {
        Result result = values[0];
        DefaultTileManager controller;
        if (reference != null && (controller = reference.get()) != null) {
            if (result.bitmap != null)
                controller.bitmapLoadSuccess(result.point, result.bytes, result.bitmap);
            else
                controller.bitmapLoadFail(result.point, result.errorType);
        }
    }

    public static class Result {
        public Result(Point point) {
            this.point = point;
        }

        Point point;
        byte[] bytes;
        Bitmap bitmap;
        ErrorMessage.ErrorType errorType;
    }
}

