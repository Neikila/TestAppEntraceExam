package com.example.neikila.mapviewerwithcache.myCustomMapView;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.neikila.mapviewerwithcache.myCustomMapView.asyncTasks.GetBitmapAsyncTask;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cache.Cache;
import cache.CacheLUImpl;
import cache.CacheManager;
import utils.messages.ErrorMessage;

public class DefaultTileManager extends MyCustomMapView.TileManager {
    private final Set<Point> pointsRequested;
    private Cache<Point, byte[]> cache;
    private final ReusableBitmapFactory bpFactory;
    private final ExecutorService pool;
    private ErrorHandler errorHandler;

    private Rect limit;
    private Point initCenterPos;
    private Point tileSize;
    private UrlProvider urlProvider;

    private boolean isNetLocked;

    public DefaultTileManager(int tileWidth, int tileHeight) {
        pool = Executors.newFixedThreadPool(5);
        pointsRequested = new HashSet<>();

        bpFactory = new ReusableBitmapFactory();
        tileSize = new Point(tileWidth, tileHeight);
        errorHandler = new CountTimeoutErrorHandler(3, this);
        isNetLocked = false;
    }

    public DefaultTileManager setInitCenterPos(int x, int y) {
        initCenterPos = new Point(x, y);
        return this;
    }

    public DefaultTileManager setInitCenterPosFromTileCoordinates(int x, int y) {
        initCenterPos = new Point(x * tileSize.x, y * tileSize.y);
        return this;
    }

    public DefaultTileManager setLimits(Rect rect) {
        limit = new Rect(rect);
        return this;
    }

    public DefaultTileManager setLimits(int leftLimit, int topLimit, int rightLimit, int bottomLimit) {
        limit = new Rect(leftLimit, topLimit, rightLimit, bottomLimit);
        return this;
    }

    public DefaultTileManager setCache(Cache<Point, byte[]> cache) {
        this.cache = cache;
        return this;
    }

    public DefaultTileManager setDefaultCache(int cacheSizeInBytes, @NonNull String cacheName) {
        CacheManager manager = CacheManager.getInstance();

        if (manager.contains(cacheName))
            cache = (Cache<Point, byte[]>) manager.get(cacheName);
        else {
            cache = new CacheLUImpl<Point, byte[]>(cacheSizeInBytes) {
                @Override
                protected int getSizeOf(byte[] el) {
                    return el.length;
                }
            };

            manager.put(cacheName, cache);
        }
        return this;
    }

    public DefaultTileManager setUrlProvider(UrlProvider provider) {
        this.urlProvider = provider;
        return this;
    }

    public DefaultTileManager setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    @Override
    public void onDetach() {
        pool.shutdown();
        bpFactory.clear();
    }

    @Override
    public Bitmap get(Point point) {
        return bpFactory.createBitmapFor(cache.get(point));
    }
    @Override
    public void setReusableBitmapAmount(int amount, int bitmapWidth, int bitmapHeight) {
        bpFactory.setPreparedBitmapsLimit(amount, bitmapWidth, bitmapHeight);
    }

    @Override
    public void bitmapFree(Bitmap bp) {
        bpFactory.free(bp);
    }

    @Override
    public void onViewPointChanged(Rect before, Rect actual) {
        final Rect beforeTmp = new Rect(before);
        final Rect actualTmp = new Rect(actual);

        Point cachedPoint = new Point();
        List<Point> pointsToLoad = new LinkedList<>();

        if (actualTmp.left < beforeTmp.left)
            for (int i = actualTmp.bottom; i <= actualTmp.top; ++i) {
                cachedPoint.set(actualTmp.left, i);
                if (!draw(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (actualTmp.right > beforeTmp.right)
            for (int i = actualTmp.bottom; i <= actualTmp.top; ++i) {
                cachedPoint.set(actualTmp.right, i);
                if (!draw(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (actualTmp.top > beforeTmp.top)
            for (int i = actualTmp.left; i <= actualTmp.right; ++i) {
                cachedPoint.set(i, actualTmp.top);
                if (!draw(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (actualTmp.bottom < beforeTmp.bottom)
            for (int i = actualTmp.left; i <= actualTmp.right; ++i) {
                cachedPoint.set(i, actualTmp.bottom);
                if (!draw(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (pointsToLoad.size() > 0)
            load(pointsToLoad);

        if (vMapView != null)
            vMapView.invalidate();
    }

    private boolean draw(Point point) {
        final Bitmap bp = bpFactory.createBitmapFor(cache.get(point));
        if (bp != null) {
            if (vMapView != null)
                vMapView.drawBitmap(bp, point);
            return true;
        }
        else
            return false;
    }

    @Override
    public boolean exist(Point point) {
        return canMoveDownTo(point.y) &&
                canMoveUpTo(point.y) &&
                canMoveLeftTo(point.x) &&
                canMoveRightTo(point.x);
    }

    @Override
    public boolean canMoveUpTo(int top) {
        return top <= limit.top;
    }

    @Override
    public boolean canMoveDownTo(int bottom) {
        return bottom >= limit.bottom;
    }

    @Override
    public boolean canMoveLeftTo(int left) {
        return left >= limit.left;
    }

    @Override
    public boolean canMoveRightTo(int right) {
        return right <= limit.right;
    }

    @Override
    Rect getLimits() {
        return limit;
    }

    @Override
    public Point getInitCenterPos() {
        return initCenterPos;
    }

    @Override
    public Point getTileSize() {
        return tileSize;
    }

    @Override
    void setNetLock(boolean val) {
        isNetLocked = val;
    }

    @Override
    public void load(Point point) {
        if (!isNetLocked && !pointsRequested.contains(point)) {
            pointsRequested.add(point);
            new GetBitmapAsyncTask(new WeakReference<>(this), urlProvider, point)
                    .setReusableBitmapFactory(new WeakReference<>(bpFactory))
                    .execute();
        }
    }

    @Override
    public void prepareTilesOnEdges(Rect actualRect) {
        Point cachedPoint = new Point();
        List<Point> pointsToLoad = new LinkedList<>();

        Rect rect = new Rect(actualRect);
        if (canMoveLeftTo(actualRect.left - 1))
            rect.left = actualRect.left - 1;
        if (canMoveRightTo(actualRect.right + 1))
            rect.right = actualRect.right + 1;
        if (canMoveUpTo(actualRect.top + 1))
            rect.top = actualRect.top + 1;
        if (canMoveDownTo(actualRect.bottom - 1))
            rect.bottom = actualRect.bottom - 1;

        if (rect.left != actualRect.left)
            for (int i = rect.bottom; i <= rect.top; ++i) {
                cachedPoint.set(rect.left, i);
                if (!cache.has(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (rect.right != actualRect.right)
            for (int i = rect.bottom; i <= rect.top; ++i) {
                cachedPoint.set(rect.right, i);
                if (!cache.has(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (rect.top != actualRect.top)
            for (int i = rect.left; i <= rect.right; ++i) {
                cachedPoint.set(i, rect.top);
                if (!cache.has(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (rect.bottom != actualRect.bottom)
            for (int i = rect.left; i <= rect.right; ++i) {
                cachedPoint.set(i, rect.bottom);
                if (!cache.has(cachedPoint) && !pointsRequested.contains(cachedPoint))
                    pointsToLoad.add(new Point(cachedPoint));
            }

        if (pointsToLoad.size() > 0) {
            load(pointsToLoad);
        }
    }

    private void load(List<Point> points) {
        if (!isNetLocked) {
            for(Point p: points)
                pointsRequested.add(p);
            new GetBitmapAsyncTask(new WeakReference<>(this), urlProvider, points)
                    .setReusableBitmapFactory(new WeakReference<>(bpFactory))
                    .execute();
        }
    }

    public void bitmapLoadSuccess(Point point, byte[] imageAsBytes, Bitmap bp) {
        if (!cache.has(point))
            cache.add(point, imageAsBytes);
        if (vMapView != null)
            vMapView.drawBitmap(bp, point);
        pointsRequested.remove(point);
    }

    public void bitmapLoadFail(Point point, ErrorMessage.ErrorType errorType) {
        pointsRequested.remove(point);
        errorHandler.onError(errorType, point);
    }

    public interface ErrorHandler {
        void onError(ErrorMessage.ErrorType erType, Point point);
    }

    public static class CountTimeoutErrorHandler implements ErrorHandler {
        private Map<Point, Integer> pointErrorAmount;
        private int maxRequestAttempts;
        private MyCustomMapView.TileManager tileManager;

        public CountTimeoutErrorHandler(int maxRequestAttempts, MyCustomMapView.TileManager tileManager) {
            this.pointErrorAmount = new HashMap<>();
            this.maxRequestAttempts = maxRequestAttempts;
            this.tileManager = tileManager;
        }

        @Override
        public void onError(ErrorMessage.ErrorType erType, Point point) {
            if (erType.equals(ErrorMessage.ErrorType.TimeoutException)) {
                Integer amount = pointErrorAmount.remove(point);
                if (amount == null)
                    amount = 0;
                if (amount < maxRequestAttempts)
                    tileManager.load(point);
                pointErrorAmount.put(point, ++amount);
            }
            if (erType.equals(ErrorMessage.ErrorType.NoConnection))
                tileManager.setNetLock(true);
        }
    }

    public interface UrlProvider {
        String getUrl(Point point);
    }

    public static class PatternUrlProvider implements UrlProvider {
        private String urpPattern;

        public PatternUrlProvider(String urpPattern) {
            this.urpPattern = urpPattern;
        }

        @Override
        public String getUrl(Point point) {
            return String.format(Locale.ENGLISH, urpPattern, point.x, point.y);
        }
    }
}
