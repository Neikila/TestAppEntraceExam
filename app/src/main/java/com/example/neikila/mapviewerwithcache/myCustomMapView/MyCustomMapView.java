package com.example.neikila.mapviewerwithcache.myCustomMapView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;

import com.example.neikila.mapviewerwithcache.R;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyCustomMapView extends View {

    public static final String DEFAULT_CACHE_NAME = "myCustomMapView";
    public static final int DEFAULT_TILE_HEIGHT = 256;
    public static final int DEFAULT_TILE_WIDTH = 256;

    private int halfOfContentWidth;
    private int halfOfContentHeight;

    private Rect bpToShowBounds;

    int tileWidth;
    int tileHeight;

    private Point coordBefore;
    private Point centerPos;

    private int imageWidth;
    private int imageHeight;

    private Rect actualBounds;
    Canvas actualCanvas;
    Bitmap actualImage;

    Canvas preparedCanvas;
    Bitmap preparedImage;

    final private Rect fromMainImage = new Rect();
    final private Rect toView        = new Rect();

    private Paint tileBoundsPaint;

    private Paint paintBackground;
    int backgroundColor;

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private AsyncTask redrawTask;

    TileManager tileManager;

    public MyCustomMapView(Context context) {
        super(context);
        init(null, 0);
    }

    public MyCustomMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MyCustomMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private static void attrNotSpecified(String mes) {
        throw new RuntimeException("You have to specify " + mes + " in attrs of view while not using custom tileManager");
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MyCustomMapView, defStyle, 0);

        boolean useCustomTileManager = a.getBoolean(R.styleable.MyCustomMapView_useCustomTileManager, false);

        if (!useCustomTileManager)
            setDefaultTileManageFromResources(a);

        backgroundColor = a.getColor(R.styleable.MyCustomMapView_backGroundColor, Color.BLACK);

        // listeners for moving around
        setOnTouchListener(new MyOnTouchListener());

        coordBefore = new Point();
        bpToShowBounds = new Rect();
        actualBounds = new Rect();

        paintBackground = new Paint();
        paintBackground.setColor(backgroundColor);

        if (a.hasValue(R.styleable.MyCustomMapView_tileBoundsDrawnColor)) {
            tileBoundsPaint = new Paint();
            tileBoundsPaint.setColor(a.getColor(
                    R.styleable.MyCustomMapView_tileBoundsDrawnColor,
                    Color.RED
            ));
            tileBoundsPaint.setStrokeWidth(2);
            tileBoundsPaint.setStyle(Paint.Style.STROKE);
        }

        a.recycle();
    }

    private void setDefaultTileManageFromResources(TypedArray a) {
        int cacheSizeInBytes = a.getInt(R.styleable.MyCustomMapView_cacheSizeInBytes, 4 * 1024 * 1024);
        String cacheName = a.getString(R.styleable.MyCustomMapView_cacheName);
        if (cacheName == null || cacheName.length() == 0) {
            cacheName = DEFAULT_CACHE_NAME;
            Log.w("initialising", "'cacheName' not provided in attr (or it's length == 0).\n" +
                    "Using defaultCacheName: " + DEFAULT_CACHE_NAME + "\n" +
                    "Might cause error in case defaultCacheName is already being used.\n");
        }


        if (!a.hasValue(R.styleable.MyCustomMapView_urlPattern))
            attrNotSpecified("'urlPattern'");
        String urlPattern = a.getString(R.styleable.MyCustomMapView_urlPattern);


        if (!a.hasValue(R.styleable.MyCustomMapView_tileWidth) ||
                !a.hasValue(R.styleable.MyCustomMapView_tileHeight))
            attrNotSpecified("'tileWidth' and 'tileHeight'");

        int tileWidth = a.getInt(R.styleable.MyCustomMapView_tileWidth, DEFAULT_TILE_WIDTH);
        int tileHeight = a.getInt(R.styleable.MyCustomMapView_tileHeight, DEFAULT_TILE_HEIGHT);


        int initCenterPosX = 0;
        int initCenterPosY = 0;
        if (a.hasValue(R.styleable.MyCustomMapView_initCenterTileX) &&
                a.hasValue(R.styleable.MyCustomMapView_initCenterTileY)) {
            initCenterPosX = a.getInt(R.styleable.MyCustomMapView_initCenterTileX, 0) * tileWidth;
            initCenterPosY = a.getInt(R.styleable.MyCustomMapView_initCenterTileY, 0) * tileHeight;
        } else {
            if (a.hasValue(R.styleable.MyCustomMapView_initCenterPosX) &&
                    a.hasValue(R.styleable.MyCustomMapView_initCenterPosY)) {
                initCenterPosX = a.getInt(R.styleable.MyCustomMapView_initCenterPosX, 0);
                initCenterPosY = a.getInt(R.styleable.MyCustomMapView_initCenterPosY, 0);
            } else {
                attrNotSpecified("'initCenterTileX', 'initCenterTileY' or " +
                        "'initOffsetX', 'initOffsetY'");
            }
        }


        if (!a.hasValue(R.styleable.MyCustomMapView_leftLimit) ||
                !a.hasValue(R.styleable.MyCustomMapView_rightLimit) ||
                !a.hasValue(R.styleable.MyCustomMapView_topLimit) ||
                !a.hasValue(R.styleable.MyCustomMapView_bottomLimit))
            attrNotSpecified("'leftLimit', 'topLimit', 'rightLimit' and 'bottomLimit'");
        Rect limits = new Rect();
        limits.left = a.getInt(R.styleable.MyCustomMapView_leftLimit, 0);
        limits.right = a.getInt(R.styleable.MyCustomMapView_rightLimit, 0);
        limits.top = a.getInt(R.styleable.MyCustomMapView_topLimit, 0);
        limits.bottom = a.getInt(R.styleable.MyCustomMapView_bottomLimit, 0);


        setTileManager(new DefaultTileManager(tileWidth, tileHeight)
                .setDefaultCache(cacheSizeInBytes, cacheName)
                .setInitCenterPos(initCenterPosX, initCenterPosY)
                .setUrlProvider(new DefaultTileManager.PatternUrlProvider(urlPattern))
                .setLimits(limits));
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }

    protected class MyOnTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                coordBefore.x = (int)event.getX();
                coordBefore.y = (int)event.getY();
                return true;
            } else if (action == MotionEvent.ACTION_MOVE) {
                moveTouch((int)event.getX(), (int)event.getY());
                return true;
            } else
                return false;
        }
    }

    private void moveTouch(int x, int y) {
        int deltaX = x - coordBefore.x;
        int deltaY = y - coordBefore.y;
        if (deltaX != 0 || deltaY != 0) {
            coordBefore.x = x;
            coordBefore.y = y;
            Rect newBpToShowBounds = calculateBoundsToShow(centerPos.x - deltaX, centerPos.y - deltaY);

            if (!tileManager.canMoveUpTo(newBpToShowBounds.top)) {
                deltaY = centerPos.y + halfOfContentHeight - (bpToShowBounds.top + 1) * tileHeight;
                newBpToShowBounds.top = bpToShowBounds.top;
            }
            else if (!tileManager.canMoveDownTo(newBpToShowBounds.bottom)) {
                deltaY = centerPos.y - halfOfContentHeight - bpToShowBounds.bottom * tileHeight;
                newBpToShowBounds.bottom = bpToShowBounds.bottom;
            }

            if (!tileManager.canMoveRightTo(newBpToShowBounds.right)) {
                deltaX = centerPos.x + halfOfContentWidth - (bpToShowBounds.right + 1) * tileWidth;
                newBpToShowBounds.right = bpToShowBounds.right;
            } else if (!tileManager.canMoveLeftTo(newBpToShowBounds.left)) {
                deltaX = centerPos.x - halfOfContentWidth - bpToShowBounds.left * tileWidth;
                newBpToShowBounds.left = bpToShowBounds.left;
            }

            if (deltaX != 0 || deltaY != 0) {
                centerPos.offset(-deltaX, -deltaY);
                fromMainImage.offset(-deltaX, -deltaY);

                if (!bpToShowBounds.equals(newBpToShowBounds)) {
                    tileManager.prepareTilesOnEdges(newBpToShowBounds);
                    updateBounds(newBpToShowBounds);
                    bpToShowBounds.set(newBpToShowBounds);
                }
                invalidate();
            }
        }
    }

    void swapMainImage(Canvas newCanvas, Bitmap newMainImage) {
        preparedImage = actualImage;
        actualImage = newMainImage;

        preparedCanvas = actualCanvas;
        actualCanvas = newCanvas;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        halfOfContentWidth = (w - paddingLeft - paddingRight) / 2;
        halfOfContentHeight = (h - paddingTop - paddingBottom) / 2;
        setCenterPos(centerPos);

        int centerX = paddingLeft + halfOfContentWidth;
        int centerY = paddingTop + halfOfContentHeight;

        actualBounds = calculateBounds();
        bpToShowBounds = calculateBoundsToShow();

        imageWidth = (actualBounds.right - actualBounds.left + 1) * tileWidth;
        imageHeight = (actualBounds.top - actualBounds.bottom + 1) * tileHeight;

        actualImage = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.RGB_565);
        actualImage.eraseColor(backgroundColor);
        actualCanvas = new Canvas(actualImage);

        toView.set(centerX - halfOfContentWidth, centerY - halfOfContentHeight,
                centerX + halfOfContentWidth, centerY + halfOfContentHeight);

        updateFromMainImageRect();

        preparedImage = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.RGB_565);
        preparedImage.eraseColor(backgroundColor);
        preparedCanvas = new Canvas(preparedImage);

        int amount = (actualBounds.right - actualBounds.left + 1) +
                (actualBounds.top - actualBounds.bottom + 1);
        tileManager.setReusableBitmapAmount(amount, tileWidth, tileHeight);

        prepareAllBitmaps();
        tileManager.prepareTilesOnEdges(bpToShowBounds);
    }

    private void updateFromMainImageRect() {
        int localCenterX = centerPos.x - actualBounds.left * tileWidth;
        int localCenterY = centerPos.y - actualBounds.bottom * tileHeight;

        fromMainImage.set(localCenterX - halfOfContentWidth, localCenterY - halfOfContentHeight,
                localCenterX + halfOfContentWidth, localCenterY + halfOfContentHeight);
    }

    private Rect calculateBounds() {
        Rect rect = calculateBoundsToShow();

        // Increment for one extra buffer
        rect.top++;
        rect.right++;
        return rect;
    }

    private Rect calculateBoundsToShow() {
        return calculateBoundsToShow(centerPos.x, centerPos.y);
    }

    private Rect calculateBoundsToShow(int x, int y) {
        Rect rect = new Rect();
        rect.left = (x - halfOfContentWidth)  / tileWidth;
        rect.top  = (y + halfOfContentHeight) / tileHeight;

        rect.right  = (x + halfOfContentWidth)  / tileWidth;
        rect.bottom = (y - halfOfContentHeight) / tileHeight;
        return rect;
    }

    private void updateBounds(Rect bpToShowBounds) {
        if (redrawTask == null &&
                (bpToShowBounds.left < actualBounds.left ||
                bpToShowBounds.right > actualBounds.right ||
                bpToShowBounds.top > actualBounds.top ||
                bpToShowBounds.bottom < actualBounds.bottom)) {

            redrawTask = new RedrawMainImageAsyncTask(this, bpToShowBounds).executeOnExecutor(executor);
        }
    }

    private void prepareAllBitmaps() {
        Point point = new Point();
        for (int i = actualBounds.left; i <= actualBounds.right; ++i) {
            for (int j = actualBounds.bottom; j <= actualBounds.top; ++j) {
                // can't avoid using point cause it is comfortable to use it as key in map
                point.set(i, j);

                Bitmap bp = tileManager.getOrLoad(point);
                if (bp != null)
                    drawBitmap(bp, point);
            }
        }
    }

    // instanced objects to reduce allocation during onDraw
    private Rect from = new Rect();
    private Rect to = new Rect();
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (fromMainImage.left < 0 ||
                fromMainImage.top < 0 ||
                fromMainImage.right > imageWidth ||
                fromMainImage.bottom > imageHeight) {

            // set def val
            to.set(toView);
            from.set(fromMainImage);

            // check bounds by X axis
            if (from.left < 0) {
                to.left -= from.left;
                from.left = 0;
            } else if (from.right > imageWidth) {
                // move actualImage right
                to.right -= from.right - imageWidth;
                from.right = imageWidth;
            }

            // check bounds by Y axis
            if (from.top < 0) {
                to.top -= from.top;
                from.top = 0;
            } else if (from.bottom > imageHeight) {
                to.bottom -= from.bottom - imageHeight;
                from.bottom = imageHeight;
            }

            canvas.drawRect(toView, paintBackground);
            canvas.drawBitmap(actualImage, from, to, null);
        } else
            canvas.drawBitmap(actualImage, fromMainImage, toView, null);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        tileManager.onAttach();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        tileManager.onDetach();
        tileManager.removeRefToView();

        executor.shutdown();

        if (redrawTask == null) {
            actualImage.recycle();
            preparedImage.recycle();
        }
    }

    public void drawBitmap(Bitmap image, Point point) {
        if (point.x >= actualBounds.left && point.x <= actualBounds.right &&
                point.y <= actualBounds.top && point.y >= actualBounds.bottom) {

            drawOnCanvas(actualCanvas, actualBounds, tileWidth, tileHeight, image, point, tileBoundsPaint);
            if (redrawTask != null)
                ((RedrawMainImageAsyncTask)redrawTask).addDrawn(point, image);
            else
                tileManager.bitmapFree(image);

            if (point.x >= bpToShowBounds.left && point.x <= bpToShowBounds.right &&
                    point.y >= bpToShowBounds.bottom && point.y <= bpToShowBounds.top)
                invalidate();
        }
    }

    static void drawOnCanvas(Canvas canvas, Rect bounds,
                             int tileWidth, int tileHeight,
                             Bitmap image, Point point) {
        drawOnCanvas(canvas, bounds, tileWidth, tileHeight, image, point, null);
    }

    static void drawOnCanvas(Canvas canvas, Rect bounds,
                             int tileWidth, int tileHeight,
                             Bitmap image, Point point,
                             Paint tileBoundsPaint) {
        if (image == null)
            return;

        int x = (point.x - bounds.left) * tileWidth;
        int y = (point.y - bounds.bottom) * tileHeight;
        canvas.drawBitmap(image, x, y, null);
        if (tileBoundsPaint != null)
            canvas.drawRect(x, y, x + tileWidth, y + tileHeight, tileBoundsPaint);
    }

    private void mainImageDrawn(Canvas newCanvas, Bitmap newMainImage, Rect newPreparedBounds) {
        redrawTask = null;

        swapMainImage(newCanvas, newMainImage);

        Rect tmp = new Rect(actualBounds);
        actualBounds.set(newPreparedBounds);

        tileManager.onViewPointChanged(tmp, newPreparedBounds);
        updateFromMainImageRect();

        invalidate();

        // check if is required to redraw
        Rect newBpToShowBounds = calculateBoundsToShow();
        updateBounds(newBpToShowBounds);
    }

    public void fullRedraw() {
        prepareAllBitmaps();
        tileManager.prepareTilesOnEdges(bpToShowBounds);
    }

    public Point getCenterTileCoordinate() {
        return new Point(centerPos.x / tileWidth, centerPos.y / tileHeight);
    }

    public Point getCenterPos() {
        return new Point(centerPos);
    }

    public void setCenterPos(Point point) {
        centerPos = new Point(point);
        Rect limits = tileManager.getLimits();

        centerPos.x = Math.max(limits.left * tileWidth + halfOfContentWidth, centerPos.x);
        centerPos.x = Math.min((limits.right + 1) * tileWidth - halfOfContentWidth - 1, centerPos.x);

        centerPos.y = Math.max(limits.bottom * tileHeight + halfOfContentHeight, centerPos.y);
        centerPos.y = Math.min((limits.top + 1) * tileHeight - halfOfContentHeight - 1, centerPos.y);
    }

    public TileManager getTileManager() {
        return tileManager;
    }

    public void setTileManager(TileManager tileManager) {
        this.tileManager = tileManager;
        tileManager.setMapView(this);

        Point size = tileManager.getTileSize();
        tileWidth = size.x;
        tileHeight = size.y;

        setCenterPos(tileManager.getInitCenterPos());
        if (centerPos == null)
            throw new RuntimeException("Init offset position is not set in tileManager");
    }

    public static abstract class TileManager {
        protected MyCustomMapView vMapView;

        public void setMapView(MyCustomMapView vMapView) {
            this.vMapView = vMapView;
        }

        abstract public Bitmap get(Point point);
        abstract public void load(Point point);
        public Bitmap getOrLoad(Point point) {
            Bitmap result = get(point);
            if (result == null)
                load(new Point(point));
            return result;
        }

        abstract public void prepareTilesOnEdges(Rect rect);

        abstract public void onViewPointChanged(Rect before, Rect actual);
        abstract public boolean exist(Point point);
        abstract public boolean canMoveUpTo(int top);
        abstract public boolean canMoveDownTo(int bottom);
        abstract public boolean canMoveLeftTo(int left);
        abstract public boolean canMoveRightTo(int right);
        abstract void setNetLock(boolean val);
        abstract Rect getLimits();

        abstract public Point getInitCenterPos();
        abstract public Point getTileSize();

        private void removeRefToView() {
            vMapView = null;
        }

        public void onAttach() {}
        public void onDetach() {}
        public void setReusableBitmapAmount(int amount, int bitmapWidth, int bitmapHeight) {}
        public void bitmapFree(Bitmap bp) { bp.recycle(); }
    }

    public static class RedrawMainImageAsyncTask extends AsyncTask<Void, Void, Void> {
        private WeakReference<MyCustomMapView> viewRef;

        private Canvas newCanvas;
        private Bitmap newMainImage;
        private Bitmap prevMainImage;

        private Rect prevBounds;
        private Rect newBpToShowBounds;

        private int tileWidth;
        private int tileHeight;

        private Rect from;
        private Rect to;
        private Rect newMainImageBounds;

        private int backgroundColor;
        private Paint tileBoundsPaint;

        private final List<Pair<Point, Bitmap>> drawnOnActual;

        public RedrawMainImageAsyncTask(MyCustomMapView view, Rect newBpToShowBounds) {
            this.viewRef = new WeakReference<>(view);
            this.newCanvas = view.preparedCanvas;
            this.newMainImage = view.preparedImage;
            this.prevMainImage = view.actualImage;

            this.prevBounds = view.actualBounds;
            this.newBpToShowBounds = newBpToShowBounds;

            this.tileHeight = view.tileHeight;
            this.tileWidth = view.tileWidth;

            this.backgroundColor = view.backgroundColor;
            this.tileBoundsPaint = view.tileBoundsPaint;

            drawnOnActual = new LinkedList<>();
        }

        public void addDrawn(Point point, Bitmap bp) {
            drawnOnActual.add(new Pair<>(new Point(point), bp));
        }

        @Override
        protected Void doInBackground(Void... params) {

            calculateNewBounds();

            newMainImage.eraseColor(backgroundColor);
            newCanvas.drawBitmap(prevMainImage, from, to, null);
            return null;
        }

        private void calculateNewBounds() {
            newMainImageBounds = new Rect();
            int mainImageWidth = prevMainImage.getWidth();
            int mainImageHeight = prevMainImage.getHeight();

            from = new Rect(0, 0, mainImageWidth, mainImageHeight);
            to = new Rect(0, 0, mainImageWidth, mainImageHeight);

            // check bounds by X axis
            if (newBpToShowBounds.left < prevBounds.left) {
                // move actualImage left
                from.right = mainImageWidth - tileWidth;
                to.left    = tileWidth;

                newMainImageBounds.left  = prevBounds.left  - 1;
                newMainImageBounds.right = prevBounds.right - 1;

            } else if (newBpToShowBounds.right > prevBounds.right) {
                // move actualImage right
                from.left = tileWidth;
                to.right  = mainImageWidth - tileWidth;

                newMainImageBounds.left  = prevBounds.left  + 1;
                newMainImageBounds.right = prevBounds.right + 1;
            } else {
                newMainImageBounds.left  = prevBounds.left;
                newMainImageBounds.right = prevBounds.right;
            }

            // check bounds by Y axis
            if (newBpToShowBounds.top > prevBounds.top) {
                // move actualImage down
                from.top  = tileHeight;
                to.bottom = mainImageHeight - tileHeight;

                newMainImageBounds.top    = prevBounds.top    + 1;
                newMainImageBounds.bottom = prevBounds.bottom + 1;

            } else if (newBpToShowBounds.bottom < prevBounds.bottom) {
                // move actualImage up
                from.bottom = mainImageHeight - tileHeight;
                to.top      = tileHeight;

                newMainImageBounds.top    = prevBounds.top    - 1;
                newMainImageBounds.bottom = prevBounds.bottom - 1;

            } else {
                newMainImageBounds.top    = prevBounds.top;
                newMainImageBounds.bottom = prevBounds.bottom;
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            MyCustomMapView view = viewRef.get();
            if (view != null) {
                drawMissed(view.tileManager);
                view.mainImageDrawn(newCanvas, newMainImage, newMainImageBounds);
            } else {
                if (!newMainImage.isRecycled())
                    newMainImage.recycle();
                if (!prevMainImage.isRecycled())
                    prevMainImage.recycle();
            }
        }

        private void drawMissed(TileManager controller) {
            for(Pair<Point, Bitmap> pair: drawnOnActual) {
                if (pair.first.x >= newMainImageBounds.left && pair.first.x <= newMainImageBounds.right &&
                        pair.first.y <= newMainImageBounds.top && pair.first.y >= newMainImageBounds.bottom)
                    drawOnCanvas(newCanvas, newMainImageBounds, tileWidth, tileHeight, pair.second, pair.first, tileBoundsPaint);
                controller.bitmapFree(pair.second);
            }
        }
    }
}
