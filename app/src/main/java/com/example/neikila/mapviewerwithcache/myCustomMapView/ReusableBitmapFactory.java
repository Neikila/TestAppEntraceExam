package com.example.neikila.mapviewerwithcache.myCustomMapView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

/**
 * Created by neikila on 20.09.16.
 */
public class ReusableBitmapFactory {
    private final Queue<SoftReference<Bitmap>> reusableBitmaps;
    private int maxSize;
    private final Bitmap.Config config;

    private final Object syncObj;

    public ReusableBitmapFactory() {
        reusableBitmaps = new LinkedList<>();
        maxSize = 0;
        config = Bitmap.Config.RGB_565;

        syncObj = new Object();
    }

    public BitmapFactory.Options getOptions() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inMutable = true;
        opt.inSampleSize = 1;
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        return opt;
    }

    public Bitmap createBitmapFor(byte[] imageAsBytes) {
        if (imageAsBytes == null)
            return null;

        BitmapFactory.Options options = getOptions();

        synchronized (syncObj) {
            Bitmap bp = null;
            while (bp == null && reusableBitmaps.size() > 0)
                bp = reusableBitmaps.poll().get();

            if (bp != null && bp.isMutable() && bp.getConfig().equals(options.inPreferredConfig))
                options.inBitmap = bp;
        }
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length, options);
    }

    public void clear() {
        for (SoftReference<Bitmap> bpRef : reusableBitmaps) {
            Bitmap bp = bpRef.get();
            if (bp != null) bp.recycle();
        }
        reusableBitmaps.clear();
    }

    public void setPreparedBitmapsLimit(int amount, int bpWidth, int bpHeight) {
        maxSize = amount;
        for (int i = 0; i < amount; ++i)
            reusableBitmaps.add(new SoftReference<>(Bitmap.createBitmap(bpWidth, bpHeight, config)));
    }

    public void free(Bitmap bp) {
        synchronized (syncObj) {
            if (reusableBitmaps.size() < maxSize && !bp.isRecycled())
                reusableBitmaps.add(new SoftReference<>(bp));
            else
                bp.recycle();
        }
    }

    public Bitmap.Config getConfig() {
        return config;
    }
}
