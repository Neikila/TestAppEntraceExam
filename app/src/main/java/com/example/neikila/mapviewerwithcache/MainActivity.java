package com.example.neikila.mapviewerwithcache;

import android.graphics.Point;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.neikila.mapviewerwithcache.myCustomMapView.DefaultTileManager;
import com.example.neikila.mapviewerwithcache.myCustomMapView.MyCustomMapView;

import utils.messages.ErrorMessage;

public class MainActivity extends AppCompatActivity {

    private MyCustomMapView vMapView;
    private TextView view;

    private final static String X_COORD = "x_coord";
    private final static String Y_COORD = "y_coord";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = (TextView) findViewById(R.id.errorMessage);

        vMapView = (MyCustomMapView) findViewById(R.id.my_map_view);

        // Uncomment below to see how it is possible to customise through the code
        // Note: it will ignore all settings in xml except
        // app:backgroundColor and app:tileBoundsDrawnColor
//         demonstrateCustomiseThroughTheCode();

        if (savedInstanceState != null)
            vMapView.setCenterPos(new Point(
                    savedInstanceState.getInt(X_COORD),
                    savedInstanceState.getInt(Y_COORD)
            ));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Point centerPoint = vMapView.getCenterPos();
        outState.putInt(X_COORD, centerPoint.x);
        outState.putInt(Y_COORD, centerPoint.y);
    }

    private void demonstrateCustomiseThroughTheCode() {
        Point offset = new Point(33198, 22539); // offset to some France city

        DefaultTileManager controller = new DefaultTileManager(256, 256)
                .setDefaultCache(4 * 1024 * 1024, "myCustomMapView")
                .setLimits(offset.x - 5, offset.y + 5, offset.x + 5, offset.y - 5)
                .setInitCenterPosFromTileCoordinates(offset.x, offset.y)
                .setUrlProvider(new DefaultTileManager.PatternUrlProvider(
                        "http://b.tile.opencyclemap.org/cycle/16/%d/%d.png"));

        controller.setErrorHandler(new DefaultTileManager.CountTimeoutErrorHandler(3, controller) {
            @Override
            public void onError(ErrorMessage.ErrorType erType, Point point) {
                super.onError(erType, point);
                handleError(erType);
                Log.d("ErrorHandler", "Error: " + erType + " for point: " + point);
            }
        });

        vMapView.setTileManager(controller);
    }

    // created only on purpose of showing simple customization
    private void handleError(ErrorMessage.ErrorType errorType) {
        int mesId = R.string.default_error_message;

        if (errorType.equals(ErrorMessage.ErrorType.NoConnection))
            mesId = R.string.no_connection;
        if (errorType.equals(ErrorMessage.ErrorType.UnknownHost))
            mesId = R.string.wrong_url;
        if (errorType.equals(ErrorMessage.ErrorType.InternetError))
            mesId = R.string.internet_error;
        view.setText(getString(mesId));
    }
}
