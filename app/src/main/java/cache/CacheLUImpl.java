package cache;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Cache used Last used strategy to select Bitmap to delete when cache size is exceeded
 */
abstract public class CacheLUImpl<T, V> implements Cache<T, V> {

    private Map<T, V> map;
    private List<T> lastUsed;

    private int maxSize;
    private int memoryUsed;

    public CacheLUImpl(int maxSize) {
        if (maxSize < 1)
            maxSize = 1;
        this.maxSize = maxSize;
        map = new HashMap<>();
        lastUsed = new LinkedList<>();
        memoryUsed = 0;
    }

    @Override
    public boolean has(T key) {
        return map.containsKey(key);
    }

    @Override
    public void add(T key, V image) {
        if (map.containsKey(key))
            return;

        int size = getSizeOf(image);
        if (size > maxSize)
            return;

        while (memoryUsed + size >= maxSize)
            remove(last());
        map.put(key, image);
        lastUsed.add(0, key);
        memoryUsed += size;
    }

    @Override
    public void remove(T key) {
        V el = map.remove(key);
        if (el != null) {
            lastUsed.remove(key);
            memoryUsed -= getSizeOf(el);
        }
    }

    @Override
    public void clear() {
        map.clear();
        lastUsed.clear();
    }

    @Override
    public V get(T key) {
        V result = map.get(key);
        if (result != null) {
            lastUsed.remove(key);
            lastUsed.add(0, key);
        }
        return result;
    }

    @Override
    public int getMemoryUsed() {
        return memoryUsed;
    }

    private T last() {
        /* No need in checking for the lastUsed.size() == 0
         * because this method is called only when size == maxSize
         */
        return lastUsed.get(lastUsed.size() - 1);
    }

    abstract protected int getSizeOf(V el);
}
