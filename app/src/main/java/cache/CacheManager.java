package cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by neikila on 20.09.16.
 */
public class CacheManager {
    public static CacheManager MANAGER = new CacheManager();

    public static CacheManager getInstance() {
        return MANAGER;
    }

    private Map<String, Cache> tagToCache;

    private CacheManager() {
        tagToCache = new ConcurrentHashMap<>();
    }

    public Cache get(String tag) {
        return tagToCache.get(tag);
    }

    public boolean put(String tag, Cache cache) {
        if (!tagToCache.containsKey(tag)) {
            tagToCache.put(tag, cache);
            return true;
        } else
            return false;
    }

    public boolean contains(String key) {
        return tagToCache.containsKey(key);
    }

    public void remove(String key) {
        tagToCache.remove(key);
    }
}
