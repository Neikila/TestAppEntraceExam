package cache;

public interface Cache<T, V> {
    boolean has(T key);
    void add(T key, V bitmap);
    void remove(T key);
    void clear();
    V get(T key);
    int getMemoryUsed();
}