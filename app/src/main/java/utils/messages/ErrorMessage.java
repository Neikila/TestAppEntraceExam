package utils.messages;

import android.graphics.Point;

/**
 * Created by neikila on 13.09.16.
 */
public class ErrorMessage {
    private ErrorType type;
    private String point;

    public ErrorMessage(ErrorType type, String point) {
        this.type = type;
        this.point = point;
    }

    public String getPoint() {
        return point;
    }

    public ErrorType getType() {
        return type;
    }

    public enum ErrorType {
        NoConnection,
        UnknownHost,
        InternetError,
        TimeoutException,
        BadData,
        None
    }
}
