package utils;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Web {
    private static OkHttpClient CLIENT = new OkHttpClient();

    public static OkHttpClient getClient() { return CLIENT; }
}
