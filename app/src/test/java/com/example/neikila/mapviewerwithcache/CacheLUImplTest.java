package com.example.neikila.mapviewerwithcache;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cache.Cache;
import cache.CacheLUImpl;

import static org.junit.Assert.*;

/**
 * Created by neikila on 23.09.16.
 */
public class CacheLUImplTest {
    private static final int maxSize = 31;
    private CacheLUImpl<Integer, byte[]> cache;
    private int sizeOfCachedMessages = 10;
    private int sizeOfNewMessage = 15;
    private Integer firstKey = 1;
    private Integer secondKey = 2;
    private Integer thirdKey = 3;

    @Before
    public void setUp() throws Exception {
        cache = new CacheLUImpl<Integer, byte[]>(maxSize) {
            @Override
            protected int getSizeOf(byte[] el) {
                return el.length;
            }
        };

    }

    private void presetThreeElement() {
        cache.add(firstKey, new byte[sizeOfCachedMessages]);
        cache.add(secondKey, new byte[sizeOfCachedMessages]);
        cache.add(thirdKey, new byte[sizeOfCachedMessages]);
    }

    @After
    public void tearDown() throws Exception {
        cache.clear();
    }

    @Test
    public void addition_isCorrect() throws Exception {
        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertTrue(cache.has(key));
    }

    @Test
    public void additionIfNoSizeAvailable_isCorrect() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertTrue(cache.has(key));
    }

    @Test
    public void was_removed_first_added() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertFalse(cache.has(firstKey));
    }

    @Test
    public void last_cached_elements_still_in_memory() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfCachedMessages]);

        Assert.assertTrue(cache.has(secondKey));
        Assert.assertTrue(cache.has(thirdKey));
    }

    @Test
    public void were_removed_enough_for_new_element() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertTrue(cache.getMemoryUsed() < maxSize);
    }


    @Test
    public void were_removed_several() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        int counterOfRemoved = 0;
        if (!cache.has(firstKey)) ++counterOfRemoved;
        if (!cache.has(secondKey)) ++counterOfRemoved;
        if (!cache.has(thirdKey)) ++counterOfRemoved;
        Assert.assertEquals(counterOfRemoved, 2);
    }

    @Test
    public void not_caching_if_size_more_than_available_in_cache() throws Exception {
        int someRandomSize = 42;
        cache.add(firstKey, new byte[sizeOfCachedMessages]);

        Integer key = 4;
        cache.add(key, new byte[someRandomSize]);

        Assert.assertEquals(cache.getMemoryUsed(), sizeOfCachedMessages);
    }

    @Test
    public void correct_memory_counting() throws Exception {
        int someRandomSize = 25;

        Integer key = 4;
        cache.add(key, new byte[someRandomSize]);

        Assert.assertEquals(cache.getMemoryUsed(), someRandomSize);
    }

    @Test
    public void correct_memory_counting_on_overhelming() throws Exception {
        presetThreeElement();

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertEquals(cache.getMemoryUsed(), sizeOfCachedMessages + sizeOfNewMessage);
    }

    @Test
    public void correct_memory_counting_after_removing_elements() throws Exception {
        presetThreeElement();

        cache.remove(thirdKey);

        Assert.assertEquals(cache.getMemoryUsed(), 2 * sizeOfCachedMessages);
    }


    @Test
    public void correct_counting_of_last_access() throws Exception {
        presetThreeElement();

        cache.get(firstKey);

        Integer key = 4;
        cache.add(key, new byte[sizeOfNewMessage]);

        Assert.assertTrue(cache.has(firstKey));
        Assert.assertFalse(cache.has(secondKey));
    }
}